classdef (CaseInsensitiveProperties=true, TruncatedProperties=true) project <  matlab.mixin.SetGet & UBase_object
    %PROJECT   PROJECT class.
    %   OBJ = PROJECT(NAME, 'PARAM', VALUE) creates a project object with
    %   the following properties.
    %       name - project name;
    %       owner - name of project owner: 'Firstname Lastname'
    %       members - list of project participants (optional)
    %
    %   Every project belongs to a 'lab' (Parent) and parents 'experiments'
    %   (Children). OBJ.experimtens lists the names of experiments that are
    %   Children to the project OBJ.
    %
    %   See also LAB and EXPERIMENT.
    
    %   Balazs Hangya, Sergio Martinez-Bellver
    %   Laboratory of Systems Neuroscience, Budapest, Hungary
    %   hangya.balazs@koki.mta.hu
    
    properties (SetAccess = private)
        owner = '';   % project owner name
    end
    properties
        members = {};   % list of project member names
    end
    properties (Dependent = true, SetAccess = private)
        experiments
        experimentnames
        subjects
        subjectnames
        sessions
        sessionnames
        units
        unitnames
    end
    methods
        
        % Constructor
        function obj = project(name,varargin)
            % project(name,'Owner',str,'Members',{str1, str2, ...},'Param',Value)
            
            % Input argument check
            if nargin < 1   % name is required
                error('MATLAB:project:notEnoughInputs','Project name is a required input.')
            end
            prs = inputParser;  % parse inputs
            addRequired(prs,'name',@ischar)
            addParameter(prs,'owner','',@ischar)
            addParameter(prs,'members',{},@iscellstr)
            addParameter(prs,'Parent',[],@(s)isa(s,'lab'))
            parse(prs,name,varargin{:});
            inpprs = prs.Results;
            
            if isempty(inpprs.owner)   % owner is enforced
                inpprs.owner = input('Please provide name of the project owner. \n','s');
            end
            if isempty(inpprs.Parent)   % check Parent
                error('MATLAB:project:noParent','Project requires a valid lab as Parent.')
            end
            
            % Assign properties
            obj@UBase_object(inpprs.name,'Parent',inpprs.Parent,'Type','project')
            obj.owner = inpprs.owner;
            obj.members = inpprs.members;
            
            % Embed project in Parent lab
            lab = obj.Parent;
            lab.Children = [lab.Children obj];
        end
        
        % Get method for experiments
        function experiments = get.experiments(obj)
            experiments = get(obj,'Children');
        end
        
        % Get method for experiments
        function experimentnames = get.experimentnames(obj)
            experiment_obj = get(obj,'Children');
            experimentnames = {};
            if ~isempty(experiment_obj)
                experimentnames = {experiment_obj.name}';
            end
            experimentnames = unique(experimentnames);
        end
        
        % Get method for subjects
        function subjects = get.subjects(obj)
            experiment_obj = obj.experiments;
            subjects = [experiment_obj.Children];
        end
        
        % Get method for subjectamess
        function subjectnames = get.subjectnames(obj)
            subjects_obj = obj.subjects;
            subjectnames = {};
            if ~isempty(subjects_obj)
                subjectnames = {subjects_obj.name}';
            end
            subjectnames = unique(subjectnames);
        end
        
        % Get method for sessions
        function sessions = get.sessions(obj)
            subject_obj = obj.subjects;
            sessions = [subject_obj.Children];
        end
        
        % Get method for sessionnamess
        function sessionnames = get.sessionnames(obj)
            session_obj = obj.sessions;
            sessionnames = {};
            if ~isempty(session_obj)
                sessionnames = {session_obj.name}';
            end
            sessionnames = unique(sessionnames);
        end
        
        % Get method for units
        function units = get.units(obj)
            session_obj = obj.sessions;
            units = [session_obj.Children];
        end
        
        % Get method for unitnames
        function unitnames = get.unitnames(obj)
            unit_obj = obj.units;
            unitnames = {};
            if ~isempty(unit_obj)
                unitnames = {unit_obj.name}';
            end
            unitnames = unique(unitnames);
        end
    end
    events
    end
end