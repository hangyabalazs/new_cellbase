classdef (CaseInsensitiveProperties=true, TruncatedProperties=true) lab < matlab.mixin.SetGet & UBase_object
    %LAB   Lab class.
    %   OBJ = LAB(NAME,'PARAM',VALUE) creates a lab object with the
    %   following properties.
    %       name - lab name;
    %       labhead - name of PI, suggested format: 'Firstname Lastname'
    %       department - name of the department
    %       institute - name of the institute
    %       city - lab location
    %       country - lab location
    %       website - lab home page URL, optional
    %       email - email address as a private property for future use for
    %           sending CellBase email notifications, optional
    %
    %   See also PROJECT and EXPERIMENT.
    
    %   Balazs Hangya
    %   Laboratory of Systems Neuroscience, Budapest, Hungary
    %   hangya.balazs@koki.mta.hu

    properties (SetAccess = private)
        labhead = '';   % PI name
        department = '';   % department name
        institute = '';   % intitution name
        city = '';   % lab location, city
        country = '';   % lab location, country
        website = '';   % lab home page URL
    end
    properties (GetAccess = private, SetAccess = private)
        email = '';   % email address for CellBase notification emails
    end
    properties (Dependent = true, SetAccess = private)
        projects
        projectnames
        experiments
        experimentnames
        subjects
        subjectnames
        sessions
        sessionnames
        units
        unitnames
    end
    methods
        
        % Constructor
        function obj = lab(name,varargin)
            %lab(name,labhead,dept,inst,city,country,website,email)
            
            % Input argument check
            if nargin < 1   % name is required
                error('MATLAB:lab:notEnoughInputs','Lab name is a required input.')
            end
            prs = inputParser; % parse inputs
            addRequired(prs,'name',@ischar)
            addParameter(prs,'labhead','',@ischar)
            addParameter(prs,'department','',@ischar)
            addParameter(prs,'institute','',@ischar)
            addParameter(prs,'city','',@ischar)
            addParameter(prs,'country','',@ischar)
            addParameter(prs,'website','',@ischar)
            addParameter(prs,'email','',@ischar)
            parse(prs,name, varargin{:});
            inpprs = prs.Results;
            
            if isempty(inpprs.labhead)   % PI name is enforced
                inpprs.labhead = input('Please provide name of labhead. \n','s');
            end
            if isempty(inpprs.department)   % department is enforced
                inpprs.dept = input('Please provide department name. \n','s');
            end
            if isempty(inpprs.institute)   % institute is enforced
                inpprs.inst = input('Please provide institution name. \n','s');
            end
            if isempty(inpprs.city)   % city is enforced
                inpprs.city = input('Please provide location (city). \n','s');
            end
            if isempty(inpprs.country)   % country is enforced
                inpprs.country = input('Please provide location (country). \n','s');
            end
            if isempty(inpprs.website)   % website is optional
                inpprs.website = input('Please type website URL or hit Enter. \n','s');
            end
            if isempty(inpprs.email)   % email is optional
                inpprs.email = input('Please type email address or hit Enter. \n','s');
            end
            
            % Assign properties
            obj@UBase_object(inpprs.name,'Parent',[],'Type','lab')
            obj.labhead = inpprs.labhead;
            obj.department = inpprs.department;
            obj.institute = inpprs.institute;
            obj.city = inpprs.city;
            obj.country = inpprs.country;
            obj.website = inpprs.website;
            obj.email = inpprs.email;
        end
        
        % Get method for projects
        function projects = get.projects(obj)
            projects = get(obj,'Children');
        end
        
        % Get method for projectnames
        function projectnames = get.projectnames(obj)
            project_obj = get(obj,'Children');
            projectnames = {};
            if ~isempty(project_obj)
                projectnames = {project_obj.name}';
            end
            projectnames = unique(projectnames);
        end
        
        % Get method for experiments
        function experiments = get.experiments(obj)
            project_obj = obj.projects;
            experiments = [project_obj.Children];
        end
        
        % Get method for experiments
        function experimentnames = get.experimentnames(obj)
            experiment_obj = obj.experiments;
            experimentnames = {};
            if ~isempty(experiment_obj)
                experimentnames = {experiment_obj.name}';
            end
            experimentnames = unique(experimentnames);
        end
        
        % Get method for subjects
        function subjects = get.subjects(obj)
            experiment_obj = obj.experiments;
            subjects = [experiment_obj.Children];
        end
        
        % Get method for subjectamess
        function subjectnames = get.subjectnames(obj)
            subjects_obj = obj.subjects;
            subjectnames = {};
            if ~isempty(subjects_obj)
                subjectnames = {subjects_obj.name}';
            end
            subjectnames = unique(subjectnames);
        end
        
        % Get method for sessions
        function sessions = get.sessions(obj)
            subject_obj = obj.subjects;
            sessions = [subject_obj.Children];
        end
        
        % Get method for sessionnamess
        function sessionnames = get.sessionnames(obj)
            session_obj = obj.sessions;
            sessionnames = {};
            if ~isempty(session_obj)
                sessionnames = {session_obj.name}';
            end
            sessionnames = unique(sessionnames);
        end
        
        % Get method for units
        function units = get.units(obj)
            session_obj = obj.sessions;
            units = [session_obj.Children];
        end
        
        % Get method for unitnames
        function unitnames = get.unitnames(obj)
            unit_obj = obj.units;
            unitnames = {};
            if ~isempty(unit_obj)
                unitnames = {unit_obj.name}';
            end
            unitnames = unique(unitnames);
        end
    end
    events
    end
end