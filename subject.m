classdef (CaseInsensitiveProperties=true, TruncatedProperties=true) subject <  matlab.mixin.SetGet & UBase_object
    %SUBJECT   SUBJECT class.
    %   OBJ = SUBJECT(NAME, 'PARAM', VALUE) creates a subject object with
    %   the following properties.
    %       name - project name;
    %
    %   Every 'subject' belongs to an 'experiment' (Parent) and parents
    %   'sessions' (Children). OBJ.sessions lists the names of sessions
    %   that are Children to the subject OBJ.
    %
    %   See also EXPERIMENT and SESSION.
    
    %   Balazs Hangya
    %   Laboratory of Systems Neuroscience, Budapest, Hungary
    %   hangya.balazs@koki.mta.hu
    
   properties (Dependent = true, SetAccess = private)
        sessions
        sessionnames
        units
        unitnames
    end
    methods
        
        % Constructor
        function obj = subject(name,varargin)
            % subject(name,'Param',Value)
            
            % Input argument check
            if nargin < 1   % name is required
                error('MATLAB:subject:notEnoughInputs','Subject name is a required input.')
            end
            prs = inputParser;  % parse inputs
            addRequired(prs,'name',@ischar)
            addParameter(prs,'Parent',[],@(s)isa(s,'experiment'))
            parse(prs,name,varargin{:});
            inpprs = prs.Results;
            
            if isempty(inpprs.Parent)   % check Parent
                error('MATLAB:subject:noParent','Subject requires a valid experiment as Parent.')
            end
            
            % Assign properties
            obj@UBase_object(inpprs.name,'Parent',inpprs.Parent,'Type','subject')
            
            % Embed experiment in Parent experiment
            experiment = obj.Parent;
            experiment.Children = [experiment.Children obj];
        end
        
        % Get method for sessions
        function sessions = get.sessions(obj)
            sessions = get(obj,'Children');
        end
        
        % Get method for sessionnamess
        function sessionnames = get.sessionnames(obj)
            sessions_obj = get(obj,'Children');
            sessionnames = {};
            if ~isempty(sessions_obj)
                sessionnames = {sessions_obj.name}';
            end
            sessionnames = unique(sessionnames);
        end
        
        % Get method for units
        function units = get.units(obj)
            session_obj = obj.sessions;
            units = [session_obj.Children];
        end
        
        % Get method for unitnames
        function unitnames = get.unitnames(obj)
            unit_obj = obj.units;
            unitnames = {};
            if ~isempty(unit_obj)
                unitnames = {unit_obj.name}';
            end
            unitnames = unique(unitnames);
        end
    end
    events
    end
end