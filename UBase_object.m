classdef UBase_object <  matlab.mixin.SetGet & dynamicprops
    %UBASE_OBJECT   Superclass for UBase object types.
    %   UBASE_OBJECT defines common properties:
    %       name: object name
    %       UUID: universially unique ID for object
    %       type: object type; defaults to 'UBase_object 'for empty 
    %           UBase_object (direct call to constructor)
    %
    %   UBASE_OBJECT handles saving. Unit metadata is saved int JSON files
    %   both into a traditional hierarchical folder tree and into a dump
    %   metadata folder using UUID.
    
    %   Balazs Hangya
    %   Laboratory of Systems Neuroscience, Budapest, Hungary
    %   hangya.balazs@koki.mta.hu
    
    properties (SetAccess = private)
        name = '';    % name
        UUID = '';    % universially unique ID for object
        Type = 'UBase_object';   % object type
    end
    properties
        Parent = [];   % Parent object in object hierarchy
        Children = [];   % Children object in object hierarchy
    end
    
    methods
        
        % Constructor
        function obj = UBase_object(name,varargin)
            % UBase_object(name,'Param',Value)
            
            % Input argument check
            if nargin < 1   % name is required
                error('MATLAB:UBase_object:notEnoughInputs','UBase_object name is a required input.')
            end
            prs = inputParser;  % parse inputs
            addRequired(prs,'name',@ischar)
%             addParameter(prs,'Parent',[],@(s)isa(s,'UBase_object'))
            addParameter(prs,'Parent',[])
            addParameter(prs,'Children',[],@(s)isa(s,'UBase_object'))
            addParameter(prs,'Type','UBase_object')
            parse(prs,name,varargin{:});
            inpprs = prs.Results;
            
            % Assign properties
            obj.name = inpprs.name;
            obj.Parent = inpprs.Parent;
            obj.Children = inpprs.Children;
            obj.UUID = char(java.util.UUID.randomUUID);   % create UUID
            obj.Type = inpprs.Type;
        end
        
        % Save
        function save(obj,varargin)
            % save(obj,'pathname',pathname)
            
            % Input argument check
            prs = inputParser;  % parse inputs
            addRequired(prs,'obj')
            addParameter(prs,'pathname','',@(s)isempty(s)||exist(s,'dir'))
            parse(prs,obj,varargin{:});
            inpprs = prs.Results;
            
            % Pathname for saving (general metadata format)
            pth = fullfile(inpprs.pathname,'metadata');
            if ~exist(pth,'dir')
                mkdir(pth)
            end
            fnm = obj.UUID;
            
            % Pathname for saving (traditional format)
            if isa(obj,'unit')
                [subjectID, sessionID, tetrodeID, unitID] = cellid2tags(obj);
                pth2 = fullfile(inpprs.pathname,subjectID,sessionID,'metadata');
                if ~exist(pth2,'dir')
                    mkdir(pth2)
                end
                fnm2 = [subjectID '_' sessionID '_' num2str(tetrodeID) '_' num2str(unitID)];
            end
                        
            % Replace Parent object with UUID (both formats supported)
            if ~isstruct(obj.Parent)
                obj.Parent = struct('name',obj.Parent.name,'UUID',obj.Parent.UUID);
            end
            
            % Create JSON text
            if verLessThan('matlab','9.1')
                jtx = myJsonencode(obj);
            else
                jtx = jsonencode(obj);
            end
            
            % Write
            fid = fopen(fullfile(pth, fnm),'w');
            fwrite(fid,jtx,'char');
            fclose(fid);
            
            % Write (traditional)
            fid = fopen(fullfile(pth2, fnm2),'w');
            fwrite(fid,jtx,'char');
            fclose(fid);
        end
    end
    
end