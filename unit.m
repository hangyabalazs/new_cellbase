classdef (CaseInsensitiveProperties=true, TruncatedProperties=true) unit <  matlab.mixin.SetGet & dynamicprops & UBase_object
    %UNIT   UNIT class.
    %   OBJ = UNIT(NAME, 'PARAM', VALUE) creates a unit object with the
    %   following properties.
    %       name - project name;
    %
    %   Every 'unit' belongs to a 'session' (Parent). Unit objects do not
    %   have Children.
    %
    %   See also SUBJECT and SESSION.
    
    %   Balazs Hangya
    %   Laboratory of Systems Neuroscience, Budapest, Hungary
    %   hangya.balazs@koki.mta.hu
    
    properties
    end
    methods
        
        % Constructor
        function obj = unit(name,varargin)
            % unit(name,'Param',Value)
            
            % Input argument check
            if nargin < 1   % name is required
                error('MATLAB:unit:notEnoughInputs','Unit name is a required input.')
            end
            prs = inputParser;  % parse inputs
            addRequired(prs,'name',@ischar)
            addParameter(prs,'Parent',[],@(s)isa(s,'session'))
            parse(prs,name,varargin{:});
            inpprs = prs.Results;
            
            if isempty(inpprs.Parent)   % check Parent
                error('MATLAB:unit:noParent','Unit requires a valid session as Parent.')
            end
            
            % Assign properties
            obj@UBase_object(inpprs.name,'Parent',inpprs.Parent,'Type','unit')
            
            % Embed experiment in Parent session
            session = obj.Parent;
            session.Children = [session.Children obj];
        end
        
        function [subjectID, sessionID, tetrodeID ,unitID] = cellid2tags(obj)
            %CELLID2TAGS   Convert cell IDs to separate tags.
            %   [SUBJECTID, SESSIONID, TETRODEID, UNITID] = CELLID2TAGS(UNIT)
            %   parses unit name string into SUBJECTID, SESSIONID, TETRODEID
            %   and UNITID strings.
            
            % Convert cell ID
            cellid = char(obj.name);
            [subjectID,remain] = strtok(cellid,'_');
            [sessionID,remain] = strtok(remain(2:end),'_');
            [tetrodeunit] = sscanf(remain(2:end),'%d.%d');
            tetrodeID = tetrodeunit(1);
            unitID = tetrodeunit(2);
        end
    end
    events
    end
end