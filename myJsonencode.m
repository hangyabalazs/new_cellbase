function jtx = myJsonencode(obj)
%MYJSONENCODE   Create JSON text.
%   JTX = MYJSONENCODE(OBJ) converts OBJ into JSON text JTX. Replacement
%   for JSONENCODE for Matlab versions before R2016b.

% End-level converter
fld = fieldnames(obj);
numFields = length(fld);
pretx = '{';   % text open
intx = [];
for iF = 1:numFields
    intx1 = ['"' fld{iF} '":'];
    crf = obj.(fld{iF});   % current field data
    if isstruct(crf)   % recursion for inner structs
        intx2 = [myJsonencode(crf) ','];
    else
        intx2 = ['"' crf '",'];
    end
    intx = [intx intx1 intx2]; %#ok<AGROW> % append
end
posttx = '}';   % text close
jtx = [pretx intx(1:end-1) posttx];   % finish