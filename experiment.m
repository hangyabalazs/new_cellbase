classdef (CaseInsensitiveProperties=true, TruncatedProperties=true) experiment <  matlab.mixin.SetGet & UBase_object
    %EXPERIMENT   EXPERIMENT class.
    %   OBJ = EXPERIMENT(NAME, 'PARAM', VALUE) creates an experiment object
    %   with the following properties.
    %       name - project name;
    %       experimenter - name of the experimenter: 'Firstname Lastname'
    %
    %   Every 'experiment' belongs to a 'project' (Parent) and parents
    %   'subjects' (Children). OBJ.subjects lists the names of subjects
    %   that are Children to the experiment OBJ.
    %
    %   See also PROJECT and EXPERIMENT.
    
    %   Balazs Hangya
    %   Laboratory of Systems Neuroscience, Budapest, Hungary
    %   hangya.balazs@koki.mta.hu
    
    properties (SetAccess = private)
        experimenter = '';   % experimenter name
    end
    properties (Dependent = true, SetAccess = private)
        subjects
        subjectnames
        sessions
        sessionnames
        units
        unitnames
    end
    methods
        
        % Constructor
        function obj = experiment(name,varargin)
            % experiment(name,'Experimenter',str,'Param',Value)
            
            % Input argument check
            if nargin < 1   % name is required
                error('MATLAB:experiment:notEnoughInputs','Experiment name is a required input.')
            end
            prs = inputParser;  % parse inputs
            addRequired(prs,'name',@ischar)
            addParameter(prs,'experimenter','',@ischar)
            addParameter(prs,'Parent',[],@(s)isa(s,'project'))
            parse(prs,name,varargin{:});
            inpprs = prs.Results;
            
            if isempty(inpprs.experimenter)   % experimenter is enforced
                inpprs.experimenter = input('Please provide name of the experimenter. \n','s');
            end
            if isempty(inpprs.Parent)   % check Parent
                error('MATLAB:experiment:noParent','Experiment requires a valid project as Parent.')
            end
            
            % Assign properties
            obj@UBase_object(inpprs.name,'Parent',inpprs.Parent,'Type','experiment')
            obj.experimenter = inpprs.experimenter;
            
            % Embed experiment in Parent project
            project = obj.Parent;
            project.Children = [project.Children obj];
        end
        
        % Get method for subjects
        function subjects = get.subjects(obj)
            subjects = get(obj,'Children');
        end
        
        % Get method for subjectamess
        function subjectnames = get.subjectnames(obj)
            subjects_obj = get(obj,'Children');
            subjectnames = {};
            if ~isempty(subjects_obj)
                subjectnames = {subjects_obj.name}';
            end
            subjectnames = unique(subjectnames);
        end
        
        % Get method for sessions
        function sessions = get.sessions(obj)
            subject_obj = obj.subjects;
            sessions = [subject_obj.Children];
        end
        
        % Get method for sessionnamess
        function sessionnames = get.sessionnames(obj)
            session_obj = obj.sessions;
            sessionnames = {};
            if ~isempty(session_obj)
                sessionnames = {session_obj.name}';
            end
            sessionnames = unique(sessionnames);
        end
        
        % Get method for units
        function units = get.units(obj)
            session_obj = obj.sessions;
            units = [session_obj.Children];
        end
        
        % Get method for unitnames
        function unitnames = get.unitnames(obj)
            unit_obj = obj.units;
            unitnames = {};
            if ~isempty(unit_obj)
                unitnames = {unit_obj.name}';
            end
            unitnames = unique(unitnames);
        end
    end
    events
    end
end