%% labs

hangyalab = lab('hangyalab',...
    'labhead','Balazs Hangya',...
    'department','Department of Cellular and Network Neurobiology',...
    'institute','Insitute of Experimental Medicine',...
    'city','Budapest',...
    'country','Hungary',...
    'website','hangyalab.koki.hu',...
    'email','hangya.balazs@koki.mta.hu');

%% projects

project1 = project('NBsync','owner','Tamas Laszlovszky',...
    'Parent',hangyalab);
project2 = project('MSsync','owner','Barni Kocsis',...
    'Parent',hangyalab);
striatal_cholinergic_project = project('Striatal_ACh','owner','Balazs Hangya',...
    'Parent',hangyalab);

%% experiments

experiment1 = experiment('anest_rat','experimenter','Balazs Hangya',...
    'Parent',project2);
experiment2 = experiment('anest_mouse','experimenter','Ricsi Fiath',...
    'Parent',project2);
experiment3 = experiment('free_mouse','experimenter','Andor Domonkos',...
    'Parent',project2);
experiment4 = experiment('anest_tagging','experimenter','Sergio Martinez-Bellver',...
    'Parent',project2);
anterior_experiment = experiment('anterior_CPu_operant','experimenter','Balazs Hangya',...
    'Parent',striatal_cholinergic_project);
posterior_experiment = experiment('posterior_CPu_operant','experimenter','Balazs Hangya',...
    'Parent',striatal_cholinergic_project);

%% subjects

n023_mouse = subject('n023','Parent',posterior_experiment);
n071_mouse = subject('n071','Parent',anterior_experiment);

%% sessions

session1 = session('n023_111220a','Parent',n023_mouse);
session2 = session('n071_141204a','Parent',n071_mouse);

%% units

unit1 = unit('n023_111220a_1.1','Parent',session1);
unit2 = unit('n071_141204a_1.1','Parent',session2);

%% UBase


myUBase = UBase('myUBase','pathname','c:\Dropbox\_data\UBase\')
% addlab(myUBase,hangyalab)
% addunit(myUBase,unit1)
% addunit(myUBase,unit2)

% addsession(myUBase,session2)

% addsubject(myUBase,n023_mouse)

addexperiment(myUBase,anterior_experiment)

myUBase