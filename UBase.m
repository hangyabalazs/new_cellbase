classdef (CaseInsensitiveProperties=true, TruncatedProperties=true) UBase < matlab.mixin.SetGet
    %UBASE   UBASE class.
    %   OBJ = UBASE(NAME,'PARAM',VALUE) creates a lab object with the
    %   following properties.
    %       name - UBase name;
    %       pathname - root path for UBase
    %
    %   UBase functions as a list of units that consists a UBase. The
    %   object hierarchy is filled up from the units as dependent
    %   properties. add<object> type of methods (addunit, addsession, etc.)
    %   always add UNITs to UBASE stored in the Children property. All
    %   properties that are objects and objectnames are dependents that are
    %   built up from Children, therefore units are the only truly listed
    %   entities.
    %
    %   UBase2mat function saves the UBase object to .mat file in the UBase 
    %   root directory.
    %
    %   units2JSON function saves all units in UBase by calling the
    %   UBase_object save function.
    %
    %   See also UBASE_OBJECT.
    
    %   Balazs Hangya & Sergio Martinez-Bellver
    %   Laboratory of Systems Neuroscience, Budapest, Hungary
    %   hangya.balazs@koki.mta.hu
    
    properties (SetAccess = private)
        name   % UBase name
        UUID    % universially unique ID for lab
    end
    properties
        pathname   % UBase location
        Children = [];   % units
        verbose = true;  % set to false to suppress feedback
    end
    properties (Dependent = true, SetAccess = private)
        labnames
        labs
        projectnames
        projects
        experimentnames
        experiments
        subjectnames
        subjects
        sessionnames
        sessions
        unitnames
        units
        cellidlist
    end
    properties (Constant = true)
        Type = 'UBase';
    end
    methods
       
        % Constructor
        function obj = UBase(name,varargin)
            
            % Input argument check
            if nargin < 1   % name is required
                error('MATLAB:lab:notEnoughInputs','Lab name is a required input.')
            end
            prs = inputParser;   % parse inputs
            addRequired(prs,'name',@ischar)
            addParameter(prs,'pathname','',@(s)isempty(s)||isdir(s))
            parse(prs,name,varargin{:});
            inpprs = prs.Results;
            
            % Selecting the directory
            if isempty(inpprs.pathname)   % pathname is enforced
                datapath = pwd;
                inpprs.pathname = uigetdir(datapath,'Select UBase directory.');
                if inpprs.pathname == 0
                    disp('UBase definition canceled.')
                    return
                end
            end
            
            % Assign properties
            obj.name = inpprs.name;
            obj.pathname = inpprs.pathname;
            obj.UUID = char(java.util.UUID.randomUUID);   % create UUID
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Add unit
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Add unit to UBase
        function addunit(UBase,obj)
            
            % Input argument should be a unit object
            if ~isa(obj,'unit')
                error('MATLAB:UBase:addunit:inputCheck','addunit requires a unit object as input.')
            end
            
            % Check if unit has already been added
            cellid = obj.name;
            if ismember(cellid,UBase.cellidlist)
                if UBase.verbose
                    disp(['Unit ' cellid ' is already in ' UBase.name ': no unit added.'])
                end
            else
                
                % Add unit to UBase
                UBase.Children = [UBase.Children obj];
                if UBase.verbose
                    disp(['Unit ' cellid ' has beed added to ' UBase.name '.'])
                end
            end
        end
        
        % Get method for units
        function units = get.units(obj)
            all_obj = get(obj,'Children');
            units = findobj(all_obj,'Type','unit');
        end
        
        % Get method for unitnames
        function unitnames = get.unitnames(obj)
            all_obj = get(obj,'Children');
            unit_obj = findobj(all_obj,'Type','unit');
            unitnames = {};
            if ~isempty(unit_obj)
                unitnames = {unit_obj.name}';
            end
            unitnames = unique(unitnames);
        end
        
        % Get method for cellidlist
        function cellidlist = get.cellidlist(obj)
            cellidlist = obj.unitnames;   % tribute to CellBase
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Add session
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Add session to UBase
        function addsession(UBase,obj)
            
            % Input argument should be a session object
            if ~isa(obj,'session')
                error('MATLAB:UBase:addsession:inputCheck','addsession requires a session object as input.')
            end
            
            % Add units to UBase
            UBase.Children = [UBase.Children obj.units];
        end
        
        % Get method for sessions
        function sessions = get.sessions(obj)
            unit_obj = obj.units;  % unit objects
            sessions = [unit_obj.Parent];
        end
        
        % Get method for sessionnames
        function sessionnames = get.sessionnames(obj)
            session_obj = obj.sessions;
            sessionnames = {};
            if ~isempty(session_obj)
                sessionnames = {session_obj.name}';
            end
            sessionnames = unique(sessionnames);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Add subject
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Add subject to UBase
        function addsubject(UBase,obj)
            
            % Input argument should be a subject object
            if ~isa(obj,'subject')
                error('MATLAB:UBase:addsubject:inputCheck','addsubject requires a subject object as input.')
            end
            
            % Add units to UBase
            UBase.Children = [UBase.Children obj.units];
        end
        
        % Get method for subjects
        function subjects = get.subjects(obj)
            session_obj = obj.sessions;  % session objects
            subjects = [session_obj.Parent];
        end
        
        % Get method for subjectnames
        function subjectnames = get.subjectnames(obj)
            subject_obj = obj.subjects;
            subjectnames = {};
            if ~isempty(subject_obj)
                subjectnames = {subject_obj.name}';
            end
            subjectnames = unique(subjectnames);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Add experiment
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Add experiment to UBase
        function addexperiment(UBase,obj)
            
            % Input argument should be an experiment object
            if ~isa(obj,'experiment')
                error('MATLAB:UBase:addexperiment:inputCheck','addexperiment requires an experiment object as input.')
            end
            
            % Add units to UBase
            UBase.Children = [UBase.Children obj.units];
        end
        
        % Get method for experiments
        function experiments = get.experiments(obj)
            subject_obj = obj.subjects;  % subject objects
            experiments = [subject_obj.Parent];
        end
        
        % Get method for experimentnames
        function experimentnames = get.experimentnames(obj)
            experiment_obj = obj.experiments;
            experimentnames = {};
            if ~isempty(experiment_obj)
                experimentnames = {experiment_obj.name}';
            end
            experimentnames = unique(experimentnames);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Add project
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Add project to UBase
        function addproject(UBase,obj)
            
            % Input argument should be a project object
            if ~isa(obj,'project')
                error('MATLAB:UBase:addproject:inputCheck','addproject requires a project object as input.')
            end
            
            % Add units to UBase
            UBase.Children = [UBase.Children obj.units];
        end
        
        % Get method for projects
        function projects = get.projects(obj)
            experiment_obj = obj.experiments;  % experiment objects
            projects = [experiment_obj.Parent];
        end
        
        % Get method for experimentnames
        function projectnames = get.projectnames(obj)
            project_obj = obj.projects;
            projectnames = {};
            if ~isempty(project_obj)
                projectnames = {project_obj.name}';
            end
            projectnames = unique(projectnames);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Add lab
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Add lab to UBase
        function addlab(UBase,obj)
            
            % Input argument should be a lab object
            if ~isa(obj,'lab')
                error('MATLAB:UBase:addlab:inputCheck','addlab requires a lab object as input.')
            end
            
            % Add units to UBase
            UBase.Children = [UBase.Children obj.units];
        end
        
        % Get method for labs
        function labs = get.labs(obj)
            project_obj = obj.projects;  % project objects
            labs = [project_obj.Parent];
        end
        
        % Get method for labnames
        function labnames = get.labnames(obj)
            lab_obj = obj.labs;
            labnames = {};
            if ~isempty(lab_obj)
                labnames = {lab_obj.name}';
            end
            labnames = unique(labnames);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Save UBase to .mat file
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Save to MATLAB file
        function UBase2mat(obj)
        
            % Input argument should be a valid UBase
            if ~isa(obj,'UBase')
                error('MATLAB:UBase:UBase2mat:inputCheck','UBase2mat requires a valid UBase.')
            end
            
            % Save
            save(fullfile(obj.pathname,obj.name),'obj')
            
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Save units to .json file
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Save units to JSON file
        function Units2JSON(obj)
            
            % Input argument should be a valid UBase
            if ~isa(obj,'UBase')
                error('MATLAB:UBase:UBase2mat:inputCheck','UBase2mat requires a valid UBase.')
            end
            
            % Save units to JSON
            numUnits = length(obj.units);
            for iU = 1:numUnits
                save(obj.units(iU),'pathname',obj.pathname)
            end
            
        end
    end
    
end