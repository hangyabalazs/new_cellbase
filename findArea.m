function a = findArea(width,varargin)
p = inputParser;
defaultHeight = 1;
defaultUnits = 'inches';
defaultShape = 'rectangle';
expectedShapes = {'square','rectangle','parallelogram'};

addRequired(p,'width',@isnumeric);
addOptional(p,'owner','',)
addOptional(p,'height',defaultHeight);
addParameter(p,'Parent',[],@(s)isa(s,'lab'))
addParameter(p,'units',defaultUnits);
addParameter(p,'shape',defaultShape,...
    @(x) any(validatestring(x,expectedShapes)));

parse(p,width,varargin{:});
p.Results

a = p.Results.width .* p.Results.height;