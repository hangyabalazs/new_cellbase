classdef (CaseInsensitiveProperties=true, TruncatedProperties=true) session <  matlab.mixin.SetGet & dynamicprops & UBase_object
    %SESSION   SESSION class.
    %   OBJ = SESSION(NAME, 'PARAM', VALUE) creates a session object with
    %   the following properties.
    %       name - project name;
    %
    %   Every 'session' belongs to a 'subject' (Parent) and parents
    %   'units' (Children). OBJ.cells lists the names of units that
    %   are Children to the session OBJ.
    %
    %   See also SUBJECT and UNIT.
    
    %   Balazs Hangya
    %   Laboratory of Systems Neuroscience, Budapest, Hungary
    %   hangya.balazs@koki.mta.hu
    
    properties (Dependent = true, SetAccess = private)
        units
        unitnames
    end
    methods
        
        % Constructor
        function obj = session(name,varargin)
            % session(name,'Param',Value)
            
            % Input argument check
            if nargin < 1   % name is required
                error('MATLAB:session:notEnoughInputs','Session name is a required input.')
            end
            prs = inputParser;  % parse inputs
            addRequired(prs,'name',@ischar)
            addParameter(prs,'Parent',[],@(s)isa(s,'subject'))
            parse(prs,name,varargin{:});
            inpprs = prs.Results;
            
            if isempty(inpprs.Parent)   % check Parent
                error('MATLAB:session:noParent','Session requires a valid subject as Parent.')
            end
            
            % Assign properties
            obj@UBase_object(inpprs.name,'Parent',inpprs.Parent,'Type','session')
            
            % Embed experiment in Parent subject
            subject = obj.Parent;
            subject.Children = [subject.Children obj];
        end
        
        % Get method for units
        function units = get.units(obj)
            units = get(obj,'Children');
        end
        
        % Get method for unitnames
        function unitnames = get.unitnames(obj)
            units_obj = obj.units;
            unitnames = {};
            if ~isempty(units_obj)
                unitnames = {units_obj.name}';
            end
            unitnames = unique(unitnames);
        end
    end
    events
    end
end